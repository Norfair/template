{ mkDerivation, aeson, amazonka, amazonka-ses, autodocodec
, autoexporter, base, base64-bytestring, blaze-html, blaze-markup
, bytestring, data-default, github, http-client, http-types, lib
, monad-logger, necrork, network-uri, opt-env-conf
, opt-env-conf-test, path, path-io, persistent, persistent-sqlite
, persistent-template, pretty-show, QuickCheck, random, safe
, shakespeare, sydtest, sydtest-discover, sydtest-persistent
, sydtest-persistent-sqlite, sydtest-wai, sydtest-yesod
, template-haskell, template-stripe-client, text, time
, unordered-containers, yaml, yesod, yesod-static
}:
mkDerivation {
  pname = "template-web-server";
  version = "0.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson amazonka amazonka-ses autodocodec base base64-bytestring
    blaze-html blaze-markup bytestring data-default github http-client
    http-types monad-logger necrork opt-env-conf path path-io
    persistent persistent-sqlite persistent-template pretty-show random
    safe shakespeare template-haskell template-stripe-client text time
    unordered-containers yaml yesod yesod-static
  ];
  libraryToolDepends = [ autoexporter ];
  executableHaskellDepends = [
    base network-uri QuickCheck sydtest sydtest-yesod
  ];
  testHaskellDepends = [
    base http-client monad-logger opt-env-conf-test path path-io
    persistent persistent-sqlite QuickCheck sydtest sydtest-persistent
    sydtest-persistent-sqlite sydtest-wai sydtest-yesod
  ];
  testToolDepends = [ sydtest-discover ];
  license = "unknown";
}

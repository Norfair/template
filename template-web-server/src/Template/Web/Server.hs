{-# LANGUAGE RecordWildCards #-}

module Template.Web.Server where

import Control.Monad
import Control.Monad.Logger
import qualified Data.Text as T
import Database.Persist.Sqlite
import qualified Necrork
import Path.IO
import Template.Web.Server.Application ()
import Template.Web.Server.Constants
import Template.Web.Server.DB
import Template.Web.Server.Foundation
import Template.Web.Server.Migration
import Template.Web.Server.OptParse
import Template.Web.Server.Static
import Text.Show.Pretty
import Yesod

templateWebServer :: IO ()
templateWebServer = do
  settings@Settings {..} <- getSettings
  when development $ pPrint settings
  runStderrLoggingT $
    filterLogger (\_ ll -> ll >= setLogLevel) $
      withSqlitePoolInfo (mkSqliteConnectionInfo (T.pack "template.sqlite3")) 1 $ \pool -> do
        flip runSqlPool pool $ do
          runMigration migrateAll
          templateMigration
        sessionKeyFile <- resolveFile' "client_session_key.aes"
        Necrork.withMNotifier setNecrorkNotifierSettings $
          liftIO $
            warp setPort $
              App
                { appLogLevel = setLogLevel,
                  appSessionKeyFile = sessionKeyFile,
                  appConnectionPool = pool,
                  appGoogleAnalyticsTracking = setGoogleAnalyticsTracking,
                  appGoogleSearchConsoleVerification = setGoogleSearchConsoleVerification,
                  appStatic = templatesStatic,
                  appStripeSettings = setStripeSettings
                }

{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Template.Web.Server.DB.Price where

import Data.Text (Text)
import qualified Data.Text as T
import Data.Word
import Database.Persist
import Database.Persist.Sql
import Text.Julius
import Text.Printf

newtype Price = Price {unPrice :: Word32} -- In rappen
  deriving (Show, Eq, PersistField, PersistFieldSql)

instance ToJavascript Price where
  toJavascript = toJavascript . renderPriceText

renderPriceText :: Price -> Text
renderPriceText = T.pack . printf "%.2f" . (/ 100) . (fromIntegral :: Word32 -> Double) . unPrice

renderPriceCents :: Price -> Int
renderPriceCents = fromIntegral . unPrice

renderPriceDouble :: Price -> Double
renderPriceDouble = (/ 100) . fromIntegral . renderPriceCents

{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Template.Web.Server.DB.Secret where

import Control.Monad
import Control.Monad.IO.Class
import Data.ByteString (ByteString)
import qualified Data.ByteString as SB
import Data.ByteString.Base64 as Base64
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import Database.Persist
import Database.Persist.Sql
import System.Random
import Yesod

newtype Secret = Secret {unSecret :: ByteString}
  deriving (Show, Eq, PersistField, PersistFieldSql)

instance Read Secret where
  readsPrec _ str = case parseSecretStringBase64 str of
    Left _ -> []
    Right s -> [(s, "")]

instance PathPiece Secret where
  fromPathPiece = either (const Nothing) Just . parseSecretTextBase64
  toPathPiece = renderSecretBase64Text

generateSecret :: (MonadIO m) => m Secret
generateSecret = liftIO $ Secret . SB.pack <$> replicateM 15 randomIO

parseSecretByteStringBase64 :: ByteString -> Either String Secret
parseSecretByteStringBase64 = fmap Secret . Base64.decode

parseSecretTextBase64 :: Text -> Either String Secret
parseSecretTextBase64 = parseSecretByteStringBase64 . TE.encodeUtf8

parseSecretStringBase64 :: String -> Either String Secret
parseSecretStringBase64 = parseSecretTextBase64 . T.pack

renderSecretBase64ByteString :: Secret -> ByteString
renderSecretBase64ByteString = Base64.encode . unSecret

renderSecretBase64Text :: Secret -> Text
renderSecretBase64Text = TE.decodeUtf8 . renderSecretBase64ByteString

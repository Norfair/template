{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ViewPatterns #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Template.Web.Server.Application where

import Template.Web.Server.Foundation
import Template.Web.Server.Handler

mkYesodDispatch "App" resourcesApp

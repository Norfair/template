{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Template.Web.Server.Migration where

import Control.Monad
import Control.Monad.IO.Class
import Database.Persist
import Template.Web.Server.DB

templateMigration :: (MonadIO m) => SqlPersistT m ()
templateMigration = forM_ setupTemplates $ \t@Template {..} ->
  upsertBy
    (UniqueTemplateRepo templateUser templateRepo)
    t
    [ TemplatePrice =. templatePrice,
      TemplateName =. templateName,
      TemplateDescription =. templateDescription,
      TemplateLoc =. templateLoc
    ]

setupTemplates :: [Template]
setupTemplates =
  [ optparseTemplate,
    cliTemplate,
    tuiTemplate,
    webServerTemplate,
    apiServerTemplate,
    syncServerTemplate
  ]

optparseTemplate :: Template
optparseTemplate =
  Template
    { templateUser = "NorfairKing",
      templateRepo = "template-optparse",
      templateName = "OptParse",
      templateDescription = "Template Argument, Option, Environment, Configuration parsing",
      templatePrice = Price 2000, -- 20 CHF
      templateLoc = 1000
    }

cliTemplate :: Template
cliTemplate =
  Template
    { templateUser = "NorfairKing",
      templateRepo = "template-cli",
      templateName = "CLI",
      templateDescription = "A template implementation of a command-line interface",
      templatePrice = Price 6000, -- 60 CHF
      templateLoc = 1000
    }

tuiTemplate :: Template
tuiTemplate =
  Template
    { templateUser = "NorfairKing",
      templateRepo = "template-tui",
      templateName = "TUI",
      templateDescription = "A template implementation of a terminal user interface",
      templatePrice = Price 6000, -- 60 CHF
      templateLoc = 1000
    }

webServerTemplate :: Template
webServerTemplate =
  Template
    { templateUser = "NorfairKing",
      templateRepo = "template-web-server",
      templateName = "Web Server",
      templateDescription = "A template implementation of a web server",
      templatePrice = Price 6000, -- 60 CHF
      templateLoc = 1000
    }

apiServerTemplate :: Template
apiServerTemplate =
  Template
    { templateUser = "NorfairKing",
      templateRepo = "template-api-server-with-auth-and-cli",
      templateName = "API Server",
      templateDescription = "A template implementation of an API server with an example command-line tool to go along with it",
      templatePrice = Price 12000, -- 120 CHF
      templateLoc = 2000
    }

syncServerTemplate :: Template
syncServerTemplate =
  Template
    { templateUser = "NorfairKing",
      templateRepo = "template-local-first-app-with-sync-server",
      templateName = "Synchronisation",
      templateDescription = "A template implementation of command-line local-first application with a synchronisation server to go along with it.",
      templatePrice = Price 20000, -- 200 CHF
      templateLoc = 2000
    }

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -Wno-orphans -Wno-name-shadowing #-}

module Template.Web.Server.DB
  ( module Template.Web.Server.DB,
    module Template.Web.Server.DB.Secret,
    module Template.Web.Server.DB.Price,
    module Database.Persist,
    module Database.Persist.Sql,
  )
where

import Data.Text (Text)
import Data.Time
import Database.Persist
import Database.Persist.Sql
import Database.Persist.TH
import Template.Web.Server.DB.Price
import Template.Web.Server.DB.Secret

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  [persistLowerCase|

Template
    name Text default=''
    user Text
    repo Text
    description Text default=''
    price Price default=0
    loc Word default=0

    UniqueTemplateRepo user repo

    deriving Show
    deriving Eq


License
    template TemplateId
    userName Text
    userEmail Text Maybe
    secret Secret
    time UTCTime

    UniqueLicenseSecret template secret
    UniqueLicenseUser template userEmail !force

    deriving Show
    deriving Eq
|]

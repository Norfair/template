{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Template.Web.Server.OptParse
  ( getSettings,
    Settings (..),
    StripeSettings (..),
  )
where

import Autodocodec
import Control.Monad.Logger
import Data.Text (Text)
import qualified Necrork
import OptEnvConf
import Paths_template_web_server (version)

getSettings :: IO Settings
getSettings = runSettingsParser version "Run the https://templates.cs-syd.eu web server"

data Settings = Settings
  { setLogLevel :: !LogLevel,
    setPort :: !Int,
    setGoogleAnalyticsTracking :: !(Maybe Text),
    setGoogleSearchConsoleVerification :: !(Maybe Text),
    setStripeSettings :: !(Maybe StripeSettings),
    setNecrorkNotifierSettings :: !(Maybe Necrork.NotifierSettings)
  }
  deriving (Show)

instance HasParser Settings where
  settingsParser = parseSettings

{-# ANN parseSettings ("NOCOVER" :: String) #-}
parseSettings :: Parser Settings
parseSettings = subEnv_ "template-web-server" $ withLocalYamlConfig $ do
  let logLevelReader = eitherReader $ \case
        "Debug" -> Right LevelDebug
        "Info" -> Right LevelInfo
        "Warn" -> Right LevelWarn
        "Error" -> Right LevelError
        s -> Left $ "Unknown LogLevel: " <> show s
  setLogLevel <-
    setting
      [ help "minimal severity of log messages",
        reader logLevelReader,
        name "log-level",
        value LevelInfo,
        metavar "LOG_LEVEL"
      ]
  setPort <-
    setting
      [ help "port to serve web requests on",
        reader auto,
        name "port",
        value 8080,
        metavar "PORT"
      ]
  setGoogleAnalyticsTracking <-
    optional $
      setting
        [ help "Google analytics tracking code",
          reader str,
          name "google-analytics-tracking",
          metavar "CODE"
        ]
  setGoogleSearchConsoleVerification <-
    optional $
      setting
        [ help "Google search console verification code",
          reader str,
          name "google-search-console-verification",
          metavar "CODE"
        ]
  setStripeSettings <- optional $ subSettings "stripe"
  setNecrorkNotifierSettings <- optional $ subSettings "necrork"
  pure Settings {..}

data StripeSettings = StripeSettings
  { stripeSetPublishableKey :: !Text,
    stripeSetSecretKey :: !Text
  }
  deriving (Show)

instance HasParser StripeSettings where
  settingsParser = parseStripeSettings

{-# ANN parseStripeSettings ("NOCOVER" :: String) #-}
parseStripeSettings :: Parser StripeSettings
parseStripeSettings = do
  stripeSetPublishableKey <-
    setting
      [ help "Publishable key",
        reader str,
        name "publishable-key",
        metavar "PUBLISHABLE_KEY"
      ]
  stripeSetSecretKey <-
    setting
      [ help "Secret key",
        reader str,
        name "secret-key",
        metavar "SECRET_KEY"
      ]
  pure StripeSettings {..}

instance HasCodec LogLevel where
  codec =
    stringConstCodec
      [ (LevelDebug, "Debug"),
        (LevelInfo, "Info"),
        (LevelWarn, "Warn"),
        (LevelError, "Error")
      ]

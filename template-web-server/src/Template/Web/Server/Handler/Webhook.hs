{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Template.Web.Server.Handler.Webhook where

import qualified Amazonka as AWS
import qualified Amazonka.SES.SendEmail as SES
import qualified Amazonka.SES.Types as SES
import Control.Arrow (left)
import Control.Monad.Logger
import qualified Data.Aeson.KeyMap as KM
import Data.Aeson.Types as JSON
import qualified Data.ByteString.Base64 as Base64
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Builder as LTB
import StripeClient as Stripe
import qualified System.IO as IO
import Template.Web.Server.Handler.Import
import Text.Blaze.Html.Renderer.Text (renderHtml)
import Text.Hamlet (hamletFile)
import Text.Shakespeare.Text (textFile)

postStripeWebhookR :: Handler ()
postStripeWebhookR = do
  mHeader <- lookupHeader "Stripe-Signature"
  case mHeader of
    Nothing -> invalidArgs ["No stripe signature header"]
    Just _ -> do
      -- TODO do verification based on the header.
      e <- requireCheckJsonBody
      logDebugN $
        T.pack $
          unlines
            [ "Webhook activated:",
              ppShow e
            ]
      case eventType e of
        "checkout.session.completed" ->
          case parseEither parseJSON (toJSON (notificationEventDataObject (eventData e))) of
            Left err -> invalidArgs [T.pack err]
            Right r -> fullfillOrder r
        t -> do
          logInfoN $ "Not handling event " <> t
          pure ()

fullfillOrder :: Checkout'session -> Handler ()
fullfillOrder session = do
  logDebugN $
    T.pack $
      unlines
        [ "Fulfilling order for this checkout session:",
          ppShow session
        ]
  let failWith err = do
        logErrorN err
        invalidArgs [err]
  let metadata = case checkout'sessionMetadata session of
        Just (NonNull hm) -> hm
        _ -> KM.empty
      getMetaM :: (FromJSON a) => JSON.Key -> Handler (Maybe a)
      getMetaM k = do
        let mMetaJSON = KM.lookup k metadata
        forM mMetaJSON $ \metaJSON ->
          case parseMaybe parseJSON metaJSON of
            Nothing -> failWith $ T.pack $ unwords ["Unparsable metadata:", show k]
            Just v -> pure v
      getMeta :: (FromJSON a) => JSON.Key -> Handler a
      getMeta k = do
        mV <- getMetaM k
        case mV of
          Nothing -> failWith $ T.pack $ unwords ["Key", show k, "not found"]
          Just v -> pure v
  case KM.lookup "product" metadata of
    Just "template" -> do
      userName64 <- TE.encodeUtf8 <$> getMeta "user_name"
      case Base64.decode userName64 >>= left show . TE.decodeUtf8' of
        Left err -> failWith $ T.pack $ unlines [unwords ["Could not base64 decode user name:", show userName64], err]
        Right userName -> do
          userEmail <- getMetaM "user_email"
          templateUser <- getMeta "template_user"
          templateRepo <- getMeta "template_repo"
          let go (Entity tid t) = do
                logDebugN $
                  T.pack $
                    unlines
                      [ "Generating license for license",
                        ppShow t,
                        "for user",
                        show userName
                      ]
                now <- liftIO getCurrentTime
                secret <- generateSecret
                let license =
                      License
                        { licenseTemplate = tid,
                          licenseUserName = userName,
                          licenseUserEmail = userEmail,
                          licenseSecret = secret,
                          licenseTime = now
                        }
                l <- runDB $ upsertBy (UniqueLicenseUser tid userEmail) license []
                sendEmail t l
          withTemplate go templateUser templateRepo
    Just (String prod) -> do
      logInfoN $ "Not handling event for different product: " <> prod
      pure () -- Purchase for a different product
    _ -> do
      logInfoN "Not handling event that has no product in its metadata"
      pure ()

sendEmail :: Template -> Entity License -> Handler ()
sendEmail Template {..} (Entity _ License {..}) = case licenseUserEmail of
  Nothing ->
    logInfoN $
      T.pack $
        unwords
          [ "Not sending an email because no email address was supplied along name:",
            show licenseUserName
          ]
  Just userEmail -> do
    logInfoN $ T.pack $ unwords ["Sending email to", show licenseUserName, "at", show licenseUserEmail]
    urlRenderer <- getUrlRenderParams
    let subject = SES.newContent "Template License Purchase"
        text = SES.newContent $ LT.toStrict $ LTB.toLazyText $ $(textFile "templates/email/license-purchase.txt") urlRenderer
        html = SES.newContent $ LT.toStrict $ renderHtml $ $(hamletFile "templates/email/license-purchase.hamlet") urlRenderer
        body = SES.newBody {SES.html = Just text, SES.text = Just html}
    let message = SES.newMessage subject body
    let destination = SES.newDestination {SES.toAddresses = Just [userEmail]}
    let req =
          (SES.newSendEmail "licenses@template.cs-syd.eu" destination message)
            { SES.replyToAddresses = Just ["syd@cs-syd.eu"]
            }
    errOrResp <- liftIO $ do
      logger <- AWS.newLogger AWS.Debug IO.stdout
      discoveredEnv <- liftIO $ AWS.newEnv AWS.discover
      let awsEnv =
            if development
              then
                discoveredEnv
                  { AWS.logger = logger,
                    AWS.region = AWS.Ireland -- Just because otherwise we get problems locally.
                  }
              else
                discoveredEnv
                  { AWS.logger = logger
                  }

      AWS.runResourceT $ AWS.sendEither awsEnv req
    case errOrResp of
      Left err ->
        logErrorN $
          T.pack $
            unlines
              [ unwords
                  [ "Error while sending email to:",
                    show userEmail
                  ],
                show err
              ]
      Right response ->
        case SES.httpStatus response of
          200 -> pure ()
          _ ->
            logErrorN $
              T.pack $
                unlines
                  [ unwords
                      [ "Error while sending email to:",
                        show userEmail
                      ],
                    ppShow response
                  ]

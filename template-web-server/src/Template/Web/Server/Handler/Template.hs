{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Template.Web.Server.Handler.Template where

import qualified Data.Aeson.KeyMap as KM
import Data.Aeson.Types
import qualified Data.ByteString.Base64 as Base64
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import Network.HTTP.Client
import Safe
import StripeClient as Stripe
import Template.Web.Server.Handler.Import
import Template.Web.Server.OptParse
import Text.Blaze

getTemplateR :: Text -> Text -> Handler Html
getTemplateR = withTemplate $ \te@(Entity _ Template {..}) -> do
  let licenseTerms = $(widgetFile "license-terms")
  card <- getTemplateRepoCard te
  let minValueTime = 25 :: Double
  let defValueTime = 50 :: Double
  let maxValueTime = 200 :: Double
  let to n = roundToQuarter $ fromIntegral templateLoc / n :: Quarter
  let minTimeToWriteOwn = to 200 :: Quarter
  let defTimeToWriteOwn = to 150 :: Quarter
  let maxTimeToWriteOwn = to 100 :: Quarter
  let minSetupTime = Quarter 1 :: Quarter
  let defSetupTime = roundToQuarter $ fromIntegral templateLoc / 2000 :: Quarter
  let maxSetupTime =
        max (Quarter 2) $
          let hoursOfPrice = roundToQuarter $ renderPriceDouble templatePrice / minValueTime :: Quarter
           in minTimeToWriteOwn - hoursOfPrice - Quarter 1
  withNavBar $ do
    setCSSydTitle $ "Template: " <> toHtml templateName
    setDescriptionIdemp $ "An overview of the " <> templateName <> " template with an option to buy a license for it."
    token <- genToken
    $(widgetFile "template")

newtype Quarter = Quarter Int
  deriving (Eq, Ord, Num)

instance ToMarkup Quarter where
  toMarkup = toMarkup . quarterToString

roundToQuarter :: Double -> Quarter
roundToQuarter d = Quarter $ round $ d * 4

quarterToString :: Quarter -> String
quarterToString (Quarter w) =
  let (q, r) = quotRem w 4
   in case r of
        0 -> show q
        1 -> show q ++ ".25"
        2 -> show q ++ ".50"
        3 -> show q ++ ".75"
        _ -> show q

data BuyForm = BuyForm
  { buyFormName :: !Text,
    buyFormEmailAddress :: !(Maybe Text)
  }

buyForm :: FormInput Handler BuyForm
buyForm =
  BuyForm
    <$> ireq textField "name"
    <*> iopt emailField "email" -- This is optional here, but required in the html, for e2e tests.

postCheckoutR :: Text -> Text -> Handler Html
postCheckoutR = withTemplate $ \(Entity tid template) -> do
  bf@BuyForm {..} <- runInputPost buyForm
  mL <- runDB $ getBy $ UniqueLicenseUser tid buyFormEmailAddress
  case mL of
    Just _ -> withNavBar $(widgetFile "already-purchased")
    Nothing -> completePurchase template bf

completePurchase :: Template -> BuyForm -> Handler Html
completePurchase Template {..} BuyForm {..} = withStripeSettings $ \stripeSets -> do
  urlRenderer <- getUrlRender
  mCustomerId <- fmap join $ forM buyFormEmailAddress $ \emailAddress -> getCustomerIdByEmail stripeSets emailAddress
  let lineItems =
        [ mkPostCheckoutSessionsRequestBodyLineItems'
            { postCheckoutSessionsRequestBodyLineItems'Quantity = Just 1,
              postCheckoutSessionsRequestBodyLineItems'PriceData =
                Just $
                  (mkPostCheckoutSessionsRequestBodyLineItems'PriceData' "CHF")
                    { postCheckoutSessionsRequestBodyLineItems'PriceData'UnitAmount = Just $ renderPriceCents templatePrice,
                      postCheckoutSessionsRequestBodyLineItems'PriceData'ProductData = Just $ mkPostCheckoutSessionsRequestBodyLineItems'PriceData'ProductData' (T.unwords ["License for template", templateUser, "/", templateRepo])
                    }
            }
        ]
      successUrl = urlRenderer CheckoutSucceededR
      cancelUrl = urlRenderer CheckoutCancelledR
      customerEmailAddress = case mCustomerId of
        Nothing -> buyFormEmailAddress
        Just _ -> Nothing
  let request =
        (mkPostCheckoutSessionsRequestBody cancelUrl successUrl)
          { postCheckoutSessionsRequestBodyCustomerEmail = customerEmailAddress,
            postCheckoutSessionsRequestBodyCustomer = mCustomerId,
            postCheckoutSessionsRequestBodyLineItems = Just lineItems,
            postCheckoutSessionsRequestBodyMode = Just PostCheckoutSessionsRequestBodyMode'EnumPayment,
            postCheckoutSessionsRequestBodyMetadata =
              Just $
                KM.fromList
                  [ ("user_name", toJSON (TE.decodeUtf8 (Base64.encode (TE.encodeUtf8 buyFormName)))),
                    ("user_email", toJSON buyFormEmailAddress),
                    ("template_user", toJSON templateUser),
                    ("template_repo", toJSON templateRepo),
                    ("product", "template")
                  ]
          }
  resp <- runStripeWith stripeSets $ postCheckoutSessions request :: Handler (Response PostCheckoutSessionsResponse)
  case responseBody resp of
    PostCheckoutSessionsResponseError err -> error $ "Stripe response parse error: " <> err
    PostCheckoutSessionsResponseDefault err -> error $ "Stripe error: " <> show err
    PostCheckoutSessionsResponse200 session -> do
      let sessionId = checkout'sessionId session :: Text
      withNavBar $ do
        addScriptRemote "https://js.stripe.com/v3/"
        $(widgetFile "template-chosen")

getCustomerIdByEmail :: StripeSettings -> Text -> Handler (Maybe Text)
getCustomerIdByEmail stripeSets email = do
  let req = mkGetCustomersParameters {getCustomersParametersQueryEmail = Just email}
  resp <- runStripeWith stripeSets $ getCustomers req
  pure $ case responseBody resp of
    GetCustomersResponse200 body -> customerId <$> headMay (getCustomersResponseBody200Data body)
    _ -> Nothing

getCheckoutSucceededR :: Handler Html
getCheckoutSucceededR = withNavBar $(widgetFile "checkout-succeeded")

getCheckoutCancelledR :: Handler Html
getCheckoutCancelledR = redirect HomeR

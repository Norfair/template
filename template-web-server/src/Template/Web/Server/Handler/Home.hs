{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Template.Web.Server.Handler.Home where

import Control.Monad
import Template.Web.Server.Handler.Import

getHomeR :: Handler Html
getHomeR = do
  templates <- runDB $ selectList [] [Desc TemplateId]
  templateTups <- forM templates $ \te -> (,) te <$> getTemplateRepoCard te
  defaultLayout $ do
    setTitle "CS-SYD - Haskell Templates"
    setDescriptionIdemp "Haskell templates by CS SYD, buy your template now!"
    $(widgetFile "home")

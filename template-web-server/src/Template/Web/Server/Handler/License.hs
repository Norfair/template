{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Template.Web.Server.Handler.License where

import Template.Web.Server.Handler.Import

getLicenseR :: Text -> Text -> Secret -> Handler Html
getLicenseR user repo secret = withTemplate go user repo
  where
    go :: Entity Template -> Handler Html
    go te@(Entity tid Template {..}) = do
      mLicense <- runDB $ getBy $ UniqueLicenseSecret tid secret
      case mLicense of
        Nothing -> notFound
        Just (Entity _ License {..}) -> do
          let licenseTerms = $(widgetFile "license-terms")
          card <- getTemplateRepoCard te
          withNavBar $(widgetFile "license")

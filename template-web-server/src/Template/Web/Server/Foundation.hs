{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}

module Template.Web.Server.Foundation where

import Control.Monad.Logger
import Data.Text (Text)
import Database.Persist.Sqlite
import Path
import StripeClient as Stripe
import Template.Web.Server.DB
import Template.Web.Server.OptParse
import Template.Web.Server.Static
import Template.Web.Server.Widget
import Text.Hamlet
import Yesod
import Yesod.EmbeddedStatic

data App = App
  { appLogLevel :: !LogLevel,
    appSessionKeyFile :: !(Path Abs File),
    appConnectionPool :: !ConnectionPool,
    appGoogleAnalyticsTracking :: !(Maybe Text),
    appGoogleSearchConsoleVerification :: !(Maybe Text),
    appStatic :: !EmbeddedStatic,
    appStripeSettings :: !(Maybe StripeSettings)
  }

mkYesodData "App" $(parseRoutesFile "routes.txt")

instance Yesod App where
  shouldLogIO app _ ll = pure $ ll >= appLogLevel app
  makeSessionBackend a = Just <$> defaultClientSessionBackend (60 * 24 * 365 * 10) (fromAbsFile (appSessionKeyFile a))

  defaultLayout widget = do
    app <- getYesod
    pageContent <- widgetToPageContent $ do
      addStylesheetRemote "https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css"
      addScriptRemote "https://code.jquery.com/jquery-3.5.1.slim.min.js"
      $(widgetFile "default-body")
    withUrlRenderer $(hamletFile "templates/default-page.hamlet")

instance RenderMessage App FormMessage where
  renderMessage _ _ = defaultFormMessage

getTemplateRepoCard :: Entity Template -> Handler Widget
getTemplateRepoCard (Entity _ Template {..}) = do
  pure $(widgetFile "template-repo-card")

instance YesodPersist App where
  type YesodPersistBackend App = SqlBackend
  runDB func = do
    pool <- getsYesod appConnectionPool
    runSqlPool func pool

withNavBar :: Widget -> Handler Html
withNavBar widget = defaultLayout $(widgetFile "with-nav-bar")

withTemplate :: (Entity Template -> Handler a) -> Text -> Text -> Handler a
withTemplate func user repo = do
  mt <- runDB $ getBy $ UniqueTemplateRepo user repo
  maybe notFound func mt

runStripeWith :: StripeSettings -> ClientM a -> Handler a
runStripeWith StripeSettings {..} func = do
  let stripeConf =
        Stripe.defaultConfiguration
          { configSecurityScheme = bearerAuthenticationSecurityScheme stripeSetSecretKey
          }
  liftIO $ Stripe.runWithConfiguration stripeConf func

withStripeSettings :: (StripeSettings -> Handler a) -> Handler a
withStripeSettings func = do
  mStripeSets <- getsYesod appStripeSettings
  case mStripeSets of
    Nothing -> error "No calls to stripe allowed without stripe settings."
    Just ss -> func ss

setCSSydTitle :: (MonadWidget m) => Html -> m ()
setCSSydTitle title = setTitle $ "CS SYD Haskell Templates - " <> title

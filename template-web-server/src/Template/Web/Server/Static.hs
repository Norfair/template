{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Template.Web.Server.Static where

import Template.Web.Server.Constants
import Yesod.EmbeddedStatic

mkEmbeddedStatic
  development
  "templatesStatic"
  [ embedFile "static/tom-sydney-kerckhove_logo-haskell.svg",
    embedFile "static/tom-sydney-kerckhove_github-icon.svg",
    embedFile "static/tom-sydney-kerckhove_linkedin-icon.svg",
    embedFile "static/tom-sydney-kerckhove_tiktok-icon.svg",
    embedFile "static/tom-sydney-kerckhove_twitter-icon.svg",
    embedFile "static/tom-sydney-kerckhove_youtube-icon.svg"
  ]

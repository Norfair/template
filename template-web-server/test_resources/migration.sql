CREATE TABLE "template"("id" INTEGER PRIMARY KEY,"name" VARCHAR NOT NULL DEFAULT '',"user" VARCHAR NOT NULL,"repo" VARCHAR NOT NULL,"description" VARCHAR NOT NULL DEFAULT '',"price" INTEGER NOT NULL DEFAULT 0,"loc" INTEGER NOT NULL DEFAULT 0,CONSTRAINT "unique_template_repo" UNIQUE ("user","repo"));
CREATE TABLE "license"("id" INTEGER PRIMARY KEY,"template" INTEGER NOT NULL REFERENCES "template" ON DELETE RESTRICT ON UPDATE RESTRICT,"user_name" VARCHAR NOT NULL,"user_email" VARCHAR NULL,"secret" BLOB NOT NULL,"time" TIMESTAMP NOT NULL,CONSTRAINT "unique_license_secret" UNIQUE ("template","secret"),CONSTRAINT "unique_license_user" UNIQUE ("template","user_email"));

-- ATTENTION CODE REVIEWER
-- If this file has been updated, please make sure to check
-- whether this test failed before that happened:
-- "Template.Web.Server.DBSpec.Can automatically migrate from the previous database schema"
-- If this test failed beforehand, but this golden test has
-- been updated anyway, that means the current migration is
-- dangerous with respect to the current database.

{-# LANGUAGE RecordWildCards #-}

module Template.Web.ServerSpec
  ( spec,
  )
where

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger
import qualified Network.HTTP.Client as HTTP
import Path.IO
import Template.Web.Server.Application ()
import Template.Web.Server.DB (Template (..), migrateAll)
import Template.Web.Server.Foundation
import Template.Web.Server.Migration
import Template.Web.Server.Static
import Test.Syd
import Test.Syd.Path
import Test.Syd.Persistent.Sqlite
import Test.Syd.Wai (managerSpec)
import Test.Syd.Yesod

spec :: Spec
spec =
  webServerSpec $
    describe "HomeR" $ do
      it "Shows a 200" $ do
        get HomeR
        statusIs 200

      forM_ setupTemplates $ \template ->
        it (unwords ["can GET template:", show $ templateUser template, show $ templateRepo template]) $ do
          get $ TemplateR (templateUser template) (templateRepo template)
          statusIs 200

webServerSpec :: YesodSpec App -> Spec
webServerSpec = modifyMaxSuccess (`div` 20) . managerSpec . yesodSpecWithSiteSetupFunc appSetupFunc

appSetupFunc :: HTTP.Manager -> SetupFunc App
appSetupFunc _ = do
  let appLogLevel = LevelWarn
  tdir <- tempDirSetupFunc "template-web-server"
  appSessionKeyFile <- resolveFile tdir "client_session_key.aes"
  appConnectionPool <- connectionPoolSetupFunc migrateAll
  liftIO $ runSqlPool templateMigration appConnectionPool
  let appGoogleAnalyticsTracking = Nothing
  let appGoogleSearchConsoleVerification = Nothing
  let appStripeSettings = Nothing
  let appStatic = templatesStatic
  pure App {..}

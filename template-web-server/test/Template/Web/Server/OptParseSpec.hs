{-# LANGUAGE TypeApplications #-}

module Template.Web.Server.OptParseSpec (spec) where

import OptEnvConf.Test
import Template.Web.Server.OptParse
import Test.Syd

spec :: Spec
spec = do
  describe "Settings" $ do
    settingsLintSpec @Settings
    goldenSettingsReferenceDocumentationSpec @Settings "test_resources/documentation.txt" "salsa-party-web-server"
    goldenSettingsNixOptionsSpec @Settings "options.nix"

module Template.Web.Server.DBSpec (spec) where

import Template.Web.Server.DB
import Test.Syd
import Test.Syd.Persistent.Sqlite

spec :: Spec
spec = do
  sqliteMigrationSucceedsSpec "test_resources/migration.sql" migrateAll

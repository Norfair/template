{-# LANGUAGE OverloadedStrings #-}

module Main where

import Network.URI
import System.Environment
import System.Exit
import Template.Web.Server.DB (Template (..))
import Template.Web.Server.Foundation
import Template.Web.Server.Migration
import Test.QuickCheck
import Test.Syd
import Test.Syd.Yesod

main :: IO ()
main = do
  uriString <- getEnv "URI"
  case parseURI uriString of
    Nothing -> die "Make sure to set URI"
    Just uri -> sydTest $ modifyMaxSuccess (`div` 50) $ yesodE2ESpec uri e2eSpec

e2eSpec :: YesodSpec (E2E App)
e2eSpec =
  describe "HomeR" $ do
    it "GETs 200" $ do
      get HomeR
      statusIs 200
    it "can GET a template" $ \yc ->
      forAll (elements setupTemplates) $ \template ->
        runYesodClientM yc $ do
          get $ TemplateR (templateUser template) (templateRepo template)
          statusIs 200
    it "can buy a template" $ \yc ->
      forAll (elements setupTemplates) $ \template ->
        runYesodClientM yc $ do
          get $ TemplateR (templateUser template) (templateRepo template)
          statusIs 200
          request $ do
            setMethod methodPost
            setUrl $ CheckoutR (templateUser template) (templateRepo template)
            addToken
            addPostParam "name" "testuser"
          statusIs 200

-- To try the webhooks:
-- stripe trigger checkout.session.completed --add checkout_session:metadata.product=template --add checkout_session:metadata.template_user=NorfairKing --add checkout_session:metadata.template_repo=template-tui --add checkout_session:metadata.user_name=dGVzdHVzZXIK

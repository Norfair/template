{ nixosTest
, template-nixos-module-factory
}:
let
  template-production = template-nixos-module-factory {
    envname = "production";
  };

  web-port = 8081;
in
nixosTest {
  name = "template-module-test";
  nodes = {
    webserver = {
      imports = [
        template-production
      ];
      services.template.production = {
        enable = true;
        openFirewall = true;
        config = {
          log-level = "Debug";
          port = web-port;
        };
      };
    };
    client = { };
  };
  testScript = ''
    webserver.start()
    client.start()

    webserver.wait_for_unit("multi-user.target")
    client.wait_for_unit("multi-user.target")

    webserver.wait_for_unit("template-production.service")

    webserver.wait_for_open_port(${builtins.toString web-port})
    client.succeed("curl webserver:${builtins.toString web-port}")
  '';
}

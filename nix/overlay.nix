final: prev:
with final.lib;
with final.haskell.lib;
let
  stripe-spec = builtins.fetchGit {
    url = "https://github.com/stripe/openapi";
    rev = "c48cf54aab65f4966ba285bdfaf86ed52f5fb70c";
  };
  generatedStripe = final.generateOpenAPIClient {
    name = "template-stripe-client";
    configFile = ../stripe-client-gen.yaml;
    src = stripe-spec + "/openapi/spec3.yaml";
  };

in
{
  templateGeneratedStripeCode = generatedStripe;
  templateReleasePackages =
    builtins.mapAttrs
      (_: pkg: justStaticExecutables (doCheck pkg))
      final.haskellPackages.templatePackages;

  templateRelease =
    final.symlinkJoin {
      name = "template-release";
      paths = builtins.map justStaticExecutables (attrValues final.templateReleasePackages);
    };

  sqlite =
    if final.stdenv.hostPlatform.isMusl
    then prev.sqlite.overrideAttrs (_: { dontDisableStatic = true; })
    else prev.sqlite;

  haskellPackages = prev.haskellPackages.override (old: {
    overrides = composeExtensions (old.overrides or (_: _: { })) (
      self: _:
        let
          templatePackages = {
            template-stripe-client = self.callPackage (generatedStripe + "/default.nix") { };
            template-web-server = buildStrictly (overrideCabal (self.callPackage ../template-web-server { }) (old: {
              doBenchmark = true;
              enableLibraryProfiling = false;
              enableExecutableProfiling = false;
              doCheck = false;
              buildFlags = (old.buildFlags or [ ]) ++ [
                "--ghc-options=-Wincomplete-uni-patterns"
                "--ghc-options=-Wincomplete-record-updates"
                "--ghc-options=-Wpartial-fields"
                "--ghc-options=-Widentities"
                "--ghc-options=-Wredundant-constraints"
                "--ghc-options=-Wcpp-undef"
              ];
              # Ugly hack because we can't just add flags to the 'test' invocation.
              # Show test output as we go, instead of all at once afterwards.
              testTarget = (old.testTarget or "") + " --show-details=direct";
              postInstall = ''
                ${old.postInstall or ""}
                $out/bin/template-web-server --port 8080 &
                sleep 0.5
                ${final.haskellPackages.linkcheck}/bin/linkcheck http://localhost:8080
                ${final.haskellPackages.seocheck}/bin/seocheck http://localhost:8080
                ${final.killall}/bin/killall template-web-server
              '';
            }));
          };
        in
        templatePackages // {
          inherit templatePackages;
        }
    );
  });
}

{ template-web-server
, opt-env-conf
}:
{ envname
}:
{ lib, pkgs, config, ... }:
with lib;

let
  cfg = config.services.template."${envname}";

  mergeListRecursively = pkgs.callPackage ./merge-lists-recursively.nix { };
in
{
  options.services.template."${envname}" = {
    enable = mkEnableOption "Template Service";
    hosts = mkOption {
      type = types.listOf types.str;
      example = "templates.cs-syd.eu";
      description = "The host to serve web requests on";
    };
    openFirewall = mkOption {
      type = types.bool;
      default = false;
      description = "Whether to open the specified ports in the firewall";
    };
    config = mkOption {
      default = { };
      type = types.submodule {
        options = import ../template-web-server/options.nix { inherit lib; };
      };
    };
    extraConfig = mkOption {
      description = "The contents of the config file, as an attribute set. This will be translated to Yaml and put in the right place along with the rest of the options defined in this submodule.";
      default = { };
    };
  };
  config =
    let
      working-dir = "/www/template/${envname}/";
      # The web server
      web-server-working-dir = working-dir + "web-server/";
      web-server-config = with cfg; mergeListRecursively [
        cfg.config
        cfg.extraConfig
      ];
      web-server-config-file = (pkgs.formats.yaml { }).generate "intray-web-server-config" web-server-config;

      web-server-service =
        with cfg;
        optionalAttrs enable {
          "template-${envname}" = opt-env-conf.addSettingsCheckToService {
            description = "Template web server ${envname} Service";
            wantedBy = [ "multi-user.target" ];
            environment = {
              "TEMPLATE_WEB_SERVER_CONFIG_FILE" = "${web-server-config-file}";
            };
            script = ''
              mkdir -p "${web-server-working-dir}"
              cd "${web-server-working-dir}" # We use this instead of 'WorkingDirectory' in the serviceConfig because systemd will not make the workingdir if it's not there.
              ${template-web-server}/bin/template-web-server
            '';
            serviceConfig = {
              Restart = "always";
              RestartSec = 1;
              Nice = 15;
            };
            unitConfig = {
              StartLimitIntervalSec = 0;
              # ensure Restart=always is always honoured
            };
          };
        };
      web-server-host =
        optionalAttrs cfg.enable {
          "${head cfg.hosts}" = {
            enableACME = true;
            forceSSL = true;
            locations."/".proxyPass = "http://localhost:${builtins.toString cfg.config.port}";
            serverAliases = tail cfg.hosts;
          };
        };
    in
    mkIf cfg.enable {
      systemd.services = web-server-service;
      networking.firewall.allowedTCPPorts = (optional (cfg.enable && cfg.openFirewall) cfg.config.port);
      services.nginx.virtualHosts = web-server-host;
    };
}

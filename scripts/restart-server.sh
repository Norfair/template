#!/usr/bin/env bash

set -x

cd template-web-server

killall template-web-server || true

export TEMPLATE_WEB_SERVER_LOG_LEVEL=LevelDebug

template-web-server &

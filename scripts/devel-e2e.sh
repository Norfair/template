#!/usr/bin/env bash

export DEVELOPMENT=True

set -x

stack install \
  --file-watch \
  --exec='./scripts/restart-e2e.sh' \
  --no-nix-pure \
  $*

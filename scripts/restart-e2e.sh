#!/usr/bin/env bash

set -x

cd template-web-server

killall template-web-server || true

export TEMPLATE_WEB_SERVER_PORT=8080
export TEMPLATE_WEB_SERVER_LOG_LEVEL=LevelDebug

template-web-server &

sleep 0.5

export URI="http://localhost:$TEMPLATE_WEB_SERVER_PORT"
template-web-server-e2e-test

#!/usr/bin/env bash

export DEVELOPMENT=True

set -x

stack install \
  --file-watch \
  --exec='./scripts/restart-server.sh' \
  --no-nix-pure \
  $*
